package com.example.ox;



import android.app.Activity;  
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity implements OnClickListener {
	
	
	public String  strState = "O";
	
	private Button bt1;
	private Button bt2;
	private Button bt3;
	private Button bt4;
	private Button bt5;
	private Button bt6;
	private Button bt7;
	private Button bt8;
	private Button bt9;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		bt1  = (Button) findViewById(R.id.button1);
		bt2  = (Button) findViewById(R.id.button2);
		bt3  = (Button) findViewById(R.id.button3);	
		bt4  = (Button) findViewById(R.id.button4);
		bt5  = (Button) findViewById(R.id.button5);
		bt6  = (Button) findViewById(R.id.button6);	
		bt7  = (Button) findViewById(R.id.button7);
		bt8  = (Button) findViewById(R.id.button8);
		bt9  = (Button) findViewById(R.id.button9);		

		
		bt1.setOnClickListener(this);
		bt2.setOnClickListener(this);
		bt3.setOnClickListener(this);
		
		bt4.setOnClickListener(this);
		bt5.setOnClickListener(this);
		bt6.setOnClickListener(this);
		
		bt7.setOnClickListener(this);
		bt8.setOnClickListener(this);
		bt9.setOnClickListener(this);
		
	}
	
	
	public void onClick(View arg0) {
		
		
		//Set click action
		Button bt = (Button) arg0;
		if (bt.getText().toString().equalsIgnoreCase(" ")) {
			bt.setText(strState);
			// toggle state
			if (strState.endsWith("O")) {
				strState = "X";
			} else {
				strState = "O";
			}
		}		
		
		//check column winner
		if(bt1.getText().toString() == bt2.getText().toString() && bt1.getText().toString() == bt3.getText().toString() ){
			if(! bt1.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt1.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		
		if(bt4.getText().toString() == bt5.getText().toString() && bt4.getText().toString() == bt6.getText().toString() ){
			if(! bt4.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt4.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		

		if(bt7.getText().toString() == bt8.getText().toString() && bt7.getText().toString() == bt9.getText().toString() ){
			if(! bt7.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt7.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		
		//check row winner
		if(bt1.getText().toString() == bt4.getText().toString() && bt1.getText().toString() == bt7.getText().toString() ){
			if(! bt1.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt1.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		
		if(bt2.getText().toString() == bt5.getText().toString() && bt2.getText().toString() == bt8.getText().toString() ){
			if(! bt2.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt2.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		

		if(bt3.getText().toString() == bt6.getText().toString() && bt3.getText().toString() == bt9.getText().toString() ){
			if(! bt3.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt3.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		
		//check cross winner
		if(bt1.getText().toString() == bt5.getText().toString() && bt1.getText().toString() == bt9.getText().toString() ){
			if(! bt1.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt1.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		
		if(bt3.getText().toString() == bt5.getText().toString() && bt3.getText().toString() == bt7.getText().toString() ){
			if(! bt3.getText().toString().equalsIgnoreCase(" ")){
				Toast.makeText(getBaseContext(), bt3.getText().toString()+" Win."  , Toast.LENGTH_LONG).show();
				DisableButton();
			}
		}
		
		//check draw condition
		if(! bt1.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt2.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt3.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt4.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt5.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt6.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt7.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt8.getText().toString().equalsIgnoreCase(" ")
			&& 	! bt9.getText().toString().equalsIgnoreCase(" ")
				){
			Toast.makeText(getBaseContext(), " DRAW."  , Toast.LENGTH_LONG).show();
			DisableButton();
		}
		
		
		
		
	}
	
	public void DisableButton(){
		bt1.setClickable(false);
		bt2.setClickable(false);
		bt3.setClickable(false);
		bt4.setClickable(false);
		bt5.setClickable(false);
		bt6.setClickable(false);
		bt7.setClickable(false);
		bt8.setClickable(false);
		bt9.setClickable(false);		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
